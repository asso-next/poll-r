import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import PollView from "../views/Poll.vue";
import LoginView from "../views/Login.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/connexion",
    name: "Login",
    component: LoginView
  },

  {
    name: "poll-add",
    path: "/nouveau-sondage",
    component: () =>
      import(/* webpackChunkName: "poll-add" */ "@/components/poll/PollAdd.vue")
  },
  {
    name: "poll-edit",
    path: "/edit-sondage/:pollId",
    component: () =>
      import(/* webpackChunkName: "poll-add" */ "@/components/poll/PollAdd.vue")
  },
  {
    name: "polls",
    path: "/sondages",
    component: PollView
  },
  {
    name: "poll",
    path: "/sondage/:pollId",
    component: () =>
      import(/* webpackChunkName: "poll" */ "@/components/poll/Poll.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
