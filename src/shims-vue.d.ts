declare module "*.vue" {
  import Vue from "vue";
  import "vuetify/types/lib";
  export default Vue;
}
