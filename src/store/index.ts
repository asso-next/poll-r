/* eslint-disable */
import Vue from "vue";
import Vuex from "vuex";
import pollModule from "./modules/poll";
import userModule from "./modules/user";
import navigationModule from "./modules/navigation";

import { createDirectStore } from "direct-vuex";

Vue.use(Vuex);

const {
  store,
  rootActionContext,
  moduleActionContext,
  rootGetterContext,
  moduleGetterContext
} = createDirectStore({
  modules: {
    pollModule,
    userModule,
    navigationModule
  }
});

// Export the direct-store instead of the classic Vuex store.
export default store;
// The following lines enable types in the injected store '$store'.
export type AppStore = typeof store;

// The following exports will be used to enable types in the
// implementation of actions and getters.
export {
  rootActionContext,
  moduleActionContext,
  rootGetterContext,
  moduleGetterContext
};

declare module "vuex" {
  interface Store<S> {
    direct: AppStore;
  }
}
