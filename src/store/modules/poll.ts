/* eslint-disable */
import {
  defineModule,
  localGetterContext,
  localActionContext
} from "direct-vuex";

import { fireDb } from '@/services/firebase';

const pollCol = fireDb.collection('polls')

export interface Poll {
  id?: string;
  date: string;
  title: string;
  description: string;
  choices: PollChoice[];
}

export interface PollUser {
  name: string;
  date: string;
}

export interface PollChoice {
  title: string;
  maxUser: number;
  subtitle: string;
  users: PollUser[];
}

export interface PollState {
  poll: Poll;
  polls: Poll[];
  choice: {
    index: number,
    edit: boolean
  }
}

const initialState: PollState = {
  polls: [],
  poll: {
    date: "",
    title: "",
    choices: [],
    description: ""
  },
  choice: {
    index: 0,
    edit: false
  }
};

const pollModule = defineModule({
  namespaced: true,
  state: (): PollState => {
    return initialState;
  },
  getters: {
    polls(...args): Poll[] {
      const { state } = pollModuleGetterCtx(args);
      return state.polls;
    },
    poll(...args): Poll {
      const { state } = pollModuleGetterCtx(args);
      return { ...state.poll, choices: state.poll.choices };
    },
  },
  mutations: {
    SET_POLLS(state: PollState, polls: Poll[]) {
      state.polls = [...polls];
    },
    SET_CHOICE(state: PollState, { index, edit }) {
      state.choice.index = index;
      state.choice.edit = edit || false
    },
    UPDATE_POLL(state: PollState, poll: Poll) {
      state.poll = { ...poll };
    },
    ADD_NEW_CHOICE(state: PollState, { choice, index }) {
      if (index === 0) {
        state.poll.choices.unshift({ ...choice })
      } else {
        state.poll.choices.splice(index, 0, { ...choice })
      }
    },
    CREATE_POLL(state: PollState, poll: Poll) {
      state.polls.push(poll)
    },
    SET_POLL(state: PollState, poll: Poll) {
      state.poll = poll;
    },
  },
  actions: {
    async fetchPolls(context) {
      const { commit } = pollModuleActionCtx(context)
      const polls: Poll[] = []
      const pollSnap = await pollCol.get()
      pollSnap.forEach(doc => polls.push({ id: doc.id, ...doc.data() } as Poll))
      commit.SET_POLLS(polls)
    },
    async fetchPoll(context, pollId: string) {
      const { commit } = pollModuleActionCtx(context)
      const pollRef = await pollCol.doc(pollId).get()
      const pollDoc = { id: pollRef.id, ...pollRef.data() } as Poll
      commit.SET_POLL(pollDoc)
    },
    async createPoll(context, poll: Poll) {
      const { commit } = pollModuleActionCtx(context)
      const pollId = poll.title.toLowerCase().replace(' ', '-')
      await pollCol.doc(pollId).set(poll)
      const newPoll = { id: pollId, ...poll } as Poll
      // commit.CREATE_POLL(newPoll)
    },
    async addUserToChoice(context, subscribe: { index: number, user: PollUser }): Promise<any> {
      const { dispatch, state } = pollModuleActionCtx(context)
      if (!state.poll?.id) return;
      const choices = state.poll.choices
      choices[subscribe.index].users.push(subscribe.user)
      await pollCol.doc(state.poll.id).update({ choices })
      return await dispatch.fetchPoll(state.poll.id)
    },
    async updatePoll(context, poll: Poll) {
      const { commit, dispatch } = pollModuleActionCtx(context)
      const pollId = poll.id;
      if (!pollId) return;
      delete poll.id;
      await pollCol.doc(pollId).update(poll)
      await dispatch.fetchPolls()
      commit.UPDATE_POLL(initialState.poll)
    },
    async deletePoll(context, pollId: string) {
      const { dispatch } = pollModuleActionCtx(context);
      if (!pollId) return;
      await pollCol.doc(pollId).delete()
      await dispatch.fetchPolls()
    },
  }
});

export default pollModule;
const pollModuleActionCtx = (context: any) =>
  localActionContext(context, pollModule);
const pollModuleGetterCtx = (args: [any, any, any, any]) =>
  localGetterContext(args, pollModule);
