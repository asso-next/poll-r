/* eslint-disable */
import {
  defineModule,
  localGetterContext,
  localActionContext
} from "direct-vuex";
import {
  UserAuth,
  disconnect,
  connectWithEmail
} from "@/services/authentication";
import { fireAuth } from "@/services/firebase/index";

export interface UserState {
  user: firebase.User | null;
}

const initialState: UserState = {
  user: null
};

const userModule = defineModule({
  namespaced: true,
  state: (): UserState => {
    return initialState;
  },
  getters: {
    isLogIn(...args): boolean {
      const { state } = userModuleGetterCtx(args);
      return !!state.user;
    }
  },
  mutations: {
    SET_USER(state, user: firebase.User | null) {
      state.user = user;
    }
  },
  actions: {
    authAction(context) {
      const { commit } = userModuleActionCtx(context);
      fireAuth.onAuthStateChanged(user => {
        if (user) commit.SET_USER(user)
        else commit.SET_USER(null)
      })
    },
    async loginByEmail(context, payload: UserAuth) {
      const { commit } = userModuleActionCtx(context);
      const { user } = await connectWithEmail(payload);
      commit.SET_USER(user);
    },
    async logout(context) {
      const { commit } = userModuleActionCtx(context);
      await disconnect();
      commit.SET_USER(null);
    }
  }
});

export default userModule;
const userModuleActionCtx = (context: any) =>
  localActionContext(context, userModule);
const userModuleGetterCtx = (args: [any, any, any, any]) =>
  localGetterContext(args, userModule);
