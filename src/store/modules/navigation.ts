/* eslint-disable */
import {
  defineModule,
  localGetterContext,
  localActionContext
} from "direct-vuex";

export interface NavigationDrawer {
  display: boolean;
}

export interface SidePanel {
  title: string;
  display: boolean;
  componentName: string;
}

export interface NavigationState {
  sidePanel: SidePanel;
  drawer: NavigationDrawer;
}

const initialState: NavigationState = {
  sidePanel: {
    title: "",
    display: false,
    componentName: ""
  },
  drawer: {
    display: false
  }
};

const navigationModule = defineModule({
  namespaced: true,
  state: (): NavigationState => {
    return initialState;
  },
  getters: {
    drawer(...args): NavigationDrawer {
      const { state } = navigationModuleGetterCtx(args);
      return state.drawer;
    },
    sidePanel(...args): SidePanel {
      const { state } = navigationModuleGetterCtx(args);
      return state.sidePanel;
    },
  },
  mutations: {
    SET_SIDE_PANEL_DISPLAY(state, display: boolean) {
      state.sidePanel.display = display;
    },
    SET_DRAWER_DISPLAY(state, display: boolean) {
      state.drawer.display = display;
    },
    SET_SIDE_PANEL_COMPONENT(state, componentName: string) {
      state.sidePanel.componentName = componentName
    },
    SET_SIDE_PANEL_TITLE(state, title: string) {
      state.sidePanel.title = title
    }
  },
  actions: {}
});

export default navigationModule;
const navigationModuleActionCtx = (context: any) =>
  localActionContext(context, navigationModule);
const navigationModuleGetterCtx = (args: [any, any, any, any]) =>
  localGetterContext(args, navigationModule);
