// import "firebase/functions";
import "firebase/firestore";
import { fireApp } from "./app";

const fireAuth = fireApp.auth();
const fireDb = fireApp.firestore();
// const fireFct = fireApp.functions("europe-west1");

export { fireAuth, fireDb };
export { FirebaseError } from "firebase";
