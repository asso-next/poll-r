import Vuetify from "vuetify";
import { mount, createLocalVue, Wrapper } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";

describe("HelloWorld.vue", () => {
  let localVue;
  let vuetify;
  let wrapper: Wrapper<any>; // eslint-disable-line
  beforeEach(() => {
    localVue = createLocalVue(); // because of vuetify, we should use a localVue instance
    vuetify = new Vuetify();
    wrapper = mount(HelloWorld, {
      localVue,
      vuetify,
      attachTo: document.body
    });
  });
  it("renders props.msg when passed", () => {
    expect(wrapper.text()).toContain("Welcome to Vuetify");
  });
});
